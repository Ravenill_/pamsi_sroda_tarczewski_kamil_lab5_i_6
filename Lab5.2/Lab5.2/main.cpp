//generator tablic
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>

#define SIZE 1000000

int main()
{
	int rozmiar1 = 25 * SIZE / 100;
	int rozmiar2 = 50 * SIZE / 100;
	int rozmiar3 = 75 * SIZE / 100;
	int rozmiar4 = 95 * SIZE / 100;
	int rozmiar5 = 99 * SIZE / 100;
	int rozmiar6 = floor(99.7 * SIZE) / 100;

	std::string nazwa1 = std::to_string(SIZE) + "_25.txt";
	std::string nazwa2 = std::to_string(SIZE) + "_50.txt";
	std::string nazwa3 = std::to_string(SIZE) + "_75.txt";
	std::string nazwa4 = std::to_string(SIZE) + "_95.txt";
	std::string nazwa5 = std::to_string(SIZE) + "_99.txt";
	std::string nazwa6 = std::to_string(SIZE) + "_997.txt";

	std::fstream file1;
	std::fstream file2;
	std::fstream file3;
	std::fstream file4;
	std::fstream file5;
	std::fstream file6;

	file1.open(nazwa1.c_str(), std::ios::out);
	file2.open(nazwa2.c_str(), std::ios::out);
	file3.open(nazwa3.c_str(), std::ios::out);
	file4.open(nazwa4.c_str(), std::ios::out);
	file5.open(nazwa5.c_str(), std::ios::out);
	file6.open(nazwa6.c_str(), std::ios::out);

	
	for (int i = 0; i < rozmiar1; i++)
		file1 << i << "\n";
	for (int i = 0; i < SIZE - rozmiar1; i++)
		file1 << rand()%SIZE+1 << "\n";
	std::cout << rozmiar1 << " ";

	for (int i = 0; i < rozmiar2; i++)
		file2 << i << "\n";
	for (int i = 0; i < SIZE - rozmiar2; i++)
		file2 << rand() % SIZE + 1 << "\n";
	std::cout << rozmiar2 << " ";

	for (int i = 0; i < rozmiar3; i++)
		file3 << i << "\n";
	for (int i = 0; i < SIZE - rozmiar3; i++)
		file3 << rand() % SIZE + 1 << "\n";
	std::cout << rozmiar3 << " ";

	for (int i = 0; i < rozmiar4; i++)
		file4 << i << "\n";
	for (int i = 0; i < SIZE - rozmiar4; i++)
		file4 << rand() % SIZE + 1 << "\n";
	std::cout << rozmiar4 << " ";

	for (int i = 0; i < rozmiar5; i++)
		file5 << i << "\n";
	for (int i = 0; i < SIZE - rozmiar5; i++)
		file5 << rand() % SIZE + 1 << "\n";
	std::cout << rozmiar5 << " ";

	for (int i = 0; i < rozmiar6; i++)
		file6 << i << "\n";
	for (int i = 0; i < SIZE - rozmiar6; i++)
		file6 << rand() % SIZE + 1 << "\n";
	std::cout << rozmiar6 << " ";

	file1.close();
	file2.close();
	file3.close();
	file4.close();
	file5.close();
	file6.close();
	system("PAUSE");
}