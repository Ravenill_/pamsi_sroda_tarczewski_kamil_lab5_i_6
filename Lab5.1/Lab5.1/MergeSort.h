#pragma once

template<typename Type>
void mergeSort(Type tab[], int front, int end);

template <typename Type>
void mergeSortAlgorithm(Type tab[], int front, int end);

template <typename Type>
void merge(Type tab[], int front, int middle, int end);

#include "MergeSort.tpp"