template <typename Type>
void introSort(Type tab[], int front, int end)
{
	introSortAlgorithm(tab, front, (end - 1), (static_cast<int>(2 * log2(static_cast<double>(end)))));
	insertSort(tab, end);
}

template <typename Type>
void introSortAlgorithm(Type tab[], int front, int end, int depth) 
{
	int middle;
	
	if (front != end)
	{
		if (depth <= 0) //
			heapSort(tab, front, end); //heapSort
		else //quickSort
		{
			middle = partitionIntro(tab, front, end); //we� punkt podzia�u, jako �rodek tablicy i podziel na cz�ci
			if ((end - front) > 9)//je�li tablica wi�ksza od 9
			{
				introSortAlgorithm(tab, front, middle, depth - 1); //dla pierwszej cz�ci
				introSortAlgorithm(tab, middle + 1, end, depth - 1); //dla drugiej cz�ci
			}
		}
	}
}


template <typename Type>
int partitionIntro(Type tab[], int front, int end) 
{
	//przyjmij punkt na �rodku jako por�wnywacz, w argumencie funkcja lambda (lekki przerost formy, ale to dla praktyki)
	int pivot = tab[([](int front, int end)->int { return (front + end) / 2; }(front, end))];
	int f = front, e = end; //indeksy tablic

	while (true)
	{
		for (e; tab[e] > pivot; e--); //dop�ki elementy sa wieksze od point
		for (f; tab[f] < pivot; f++); //dop�ki elementy sa mniejsze od point

		if (f < e) //zamie� miejscami
			swapQuick(tab, f, e);
		else //gdy sko�czy, zwr�� �rodek jako punkt podzia�u, przerwij p�tle i zako�cz
			return e;
	}
}

template <typename Type>
void swapIntro(Type tab[], int front, int end)
{
	int temp = tab[end]; //tymaczasowa do zamieniania miejscami
	tab[end] = tab[front];
	tab[front] = temp;
}


template <typename Type>
void insertSort(Type tab[], int end) 
{
	for (int i = 1; i < end; i++)
	{
		int j; //do obrotu p�tl�
		int temp = tab[i]; //klucz tymczasowy
		for (j = i - 1; j >= 0 && tab[j] > temp; j--)
			tab[j + 1] = tab[j]; //przek�adaj tak d�ugo, by tab[j] < key
		tab[j+1] = temp; //wklej tab[i]
	}
}

template <typename Type>
void heapTool(Type tab[], int front, int end) 
{
	int temp = tab[front]; //przetrzymuje "pierwszy" element
	while (front <= (end / 2))  //kr�ci, p�ki nie dojdzie to po�owy (najwi�kszego)
	{
		int l = 2 * front;
		for (l; l < (end - front) && tab[l] < tab[l+1]; l++); //znajd� wi�kszy
		if (temp >= tab[l]) //pierwszy (wstawiany) jest wi�kszy od �rodkowego
			break;

		tab[front] = tab[l]; //zamie� miejscami, by dalej szuka�
		front = l;
	}
	tab[front] = temp; //pierwszy wraca na miejsce
}

template <typename Type>
void heapSort(Type tab[], int front, int end) 
{
	for (int i = ((end - 1) / 2); i >= front; i--)//budowa kopca
		heapTool(tab, i, end - 1);

	for (int i = (end - 1); i > front; i--)//rozbieranie
	{
		swapIntro(tab, i, front);
		heapTool(tab, front, i - 1);
	}
}