#include <iostream>
#include <cstdio>
#include "Sort.h"

#define ROZMIAR 25

int main()
{
	int tab1[ROZMIAR];
	int tab2[ROZMIAR];
	int tab3[ROZMIAR];

	for (int i = 0; i < ROZMIAR; i++)
	{
		int a = std::rand()%51;
		tab1[i] = a;
		tab2[i] = a;
		tab3[i] = a;
		std::cout << tab1[i] << " ";
	}
	std::cout << isSorted(tab1, ROZMIAR);
	std::cout << "\n" << isSorted(tab1, ROZMIAR) << "_____________________\n";

	std::cout << "\n\n\n";

	mergeSort<int>(tab1, 0, ROZMIAR);
	quickSort<int>(tab2, 0, ROZMIAR);
	introSort<int>(tab3, 0, ROZMIAR);

	//drukuj
	for (int i = 0; i < ROZMIAR; i++)
		std::cout << tab1[i] << " ";
	std::cout << "\n" << isSorted(tab1, ROZMIAR) << "_____________________\n";

	for (int i = 0; i < ROZMIAR; i++)
		std::cout << tab2[i] << " ";
	std::cout << "\n" << isSorted(tab2, ROZMIAR) << "_____________________\n";

	for (int i = 0; i < ROZMIAR; i++)
		std::cout << tab3[i] << " ";
	std::cout << "\n" << isSorted(tab3, ROZMIAR) << "_____________________\n";

	system("PAUSE");
}