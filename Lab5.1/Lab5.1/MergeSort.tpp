template<typename Type>
void mergeSort(Type tab[], int front, int end)
{
	mergeSortAlgorithm(tab, front, (end - 1));
}

template<typename Type>
void mergeSortAlgorithm(Type tab[], int front, int end)
{
	int middle = (front + end) / 2;

	if (front != end) //je�li d�u�szy ni� 1 elem
	{
		mergeSortAlgorithm(tab, front, middle); //dla pierwszej cz�ci
		mergeSortAlgorithm(tab, middle + 1, end); //dla drugiej cz�ci
		merge(tab, front, middle, end); //po��cz
	}
}

template<typename Type>
void merge(Type tab[], int front, int middle, int end)
{
	int size = end - front; //rozmiar skrawka tablicy
	int *tab_temp = new int[size+1];
	int f = front, m = middle + 1, k = 0; //indeksy tablic

	while (f <= middle && m <= end) //posortuj do tablicy temp
	{
		if (tab[m] > tab[f])
			tab_temp[k++] = tab[f++];
		else
			tab_temp[k++] = tab[m++];
	}
	while (f <= middle) //je�li pierwszy warunek si� nie sko�czy�, doko�cz
		tab_temp[k++] = tab[f++];
	while (m <= end) //je�li drugi warunek si� nie sko�czy�, doko�cz
		tab_temp[k++] = tab[m++];

	for (int i = 0; i <= size; i++) //przedrukuj na prawdziw� tablic� (dany skrawek)
		tab[front + i] = tab_temp[i];

	delete[] tab_temp;
}