//generator tablic
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>

#define SIZE 1000000

int main()
{
	std::string nazwa1 = std::to_string(SIZE) + "_100.txt";
	std::string nazwa2 = std::to_string(SIZE) + "_0.txt";

	std::fstream file1;
	std::fstream file2;

	file1.open(nazwa1.c_str(), std::ios::out);
	file2.open(nazwa2.c_str(), std::ios::out);

	for (int i = SIZE; i > 0; i--)
		file1 << i << "\n";
	std::cout << "Odwrotna \n";

	for (int i = 0; i < SIZE; i++)
		file2 << rand() % SIZE + 1 << "\n";	
	std::cout << "Losowa \n";

	file1.close();
	file2.close();

	system("PAUSE");
}