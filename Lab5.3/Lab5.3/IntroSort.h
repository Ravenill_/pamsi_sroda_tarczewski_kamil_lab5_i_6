#pragma once

template <typename Type>
void introSort(Type tab[], int front, int end);

template <typename Type>
void introSortAlgorithm(Type tab[], int front, int end, int depth);

template <typename Type>
int partitionIntro(Type tab[], int front, int end);

template <typename Type>
void insertSort(Type tab[], int end);

template <typename Type>
void swapIntro(Type tab[], int front, int end);

template <typename Type>
void heapTool(Type tab[], int front, int end);

template <typename Type>
void heapSort(Type tab[], int front, int end);

#include "IntroSort.tpp"