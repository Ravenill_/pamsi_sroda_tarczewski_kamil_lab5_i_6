#pragma once

template<typename Type>
void quickSort(Type tab[], int front, int end);

template <typename Type>
void quickSortAlgorithm(Type tab[], int front, int end);

template <typename Type>
void swapQuick(Type tab[], int &front, int &end);

template <typename Type>
int partition(Type tab[], int front, int end);

#include "QuickSort.tpp"