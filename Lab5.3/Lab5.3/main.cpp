#include "Tester.h"
#include <iostream>

int main()
{
	int SIZE[5] = { 10000, 50000, 100000, 500000, 1000000 };
	const char * SORT[6] = { "_25", "_50", "_75", "_95", "_99", "_997" };

	for (int s = 0; s < 5; s++)
	{
		for (int j = 0; j < 6; j++)
		{
			Tester tester(SIZE[s]);
			double time;

			std::fstream fileout1;
			std::fstream fileout2;
			std::fstream fileout3;
			std::string nazwaout1 = "time_" + std::to_string(SIZE[s]) + SORT[j] + "_merge" + ".txt";
			std::string nazwaout2 = "time_" + std::to_string(SIZE[s]) + SORT[j] + "_quick" + ".txt";
			std::string nazwaout3 = "time_" + std::to_string(SIZE[s]) + SORT[j] + "_intro" + ".txt";

			fileout1.open(nazwaout1.c_str(), std::ios::out);

			for (int i = 1; i <= 100; i++)
			{
				time = tester.tabm(SORT[j]);
				fileout1 << time << "\n";
				std::cout << "Mergesort, tablica " << i << ", rozmiar: " << SIZE[s] << ", sortowanie: " << SORT[j] << "%, czas: " << time << "\n";
			}
			fileout1.close();
			std::cout << "\n";

			fileout2.open(nazwaout2.c_str(), std::ios::out);

			for (int i = 1; i <= 100; i++)
			{
				time = tester.tabq(SORT[j]);
				fileout2 << time << "\n";
				std::cout << "Quicksort, tablica " << i << ", rozmiar: " << SIZE[s] << ", sortowanie: " << SORT[j] << "%, czas: " << time << "\n";
			}
			fileout2.close();
			std::cout << "\n";

			fileout3.open(nazwaout3.c_str(), std::ios::out);

			for (int i = 1; i <= 100; i++)
			{
				time = tester.tabi(SORT[j]);
				fileout3 << time << "\n";
				std::cout << "Introsort, tablica " << i << ", rozmiar: " << SIZE[s] << ", sortowanie: " << SORT[j] << "%, czas: " << time << "\n";
			}
			fileout3.close();
			std::cout << "\n";
		}
		std::cout << "\n__________________\n";
	}
	system("PAUSE");
}