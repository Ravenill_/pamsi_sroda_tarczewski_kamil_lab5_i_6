#pragma once


class Timer
{
	double PCFreq = 0.0;
	__int64 CounterStart = 0;

public:
	void StartCounter();   // Ustawia odliczanie na zero
	double GetCounter();     // Zwraca czas od ostatniego wywolania StartCounter();
};
