#include "Tester.h"
#include "Timer.h"
#include "Sort.h"
#include <cstdlib>
#include <iostream>

Tester::Tester(int a)
{
	size = a;
}

double Tester::tabm(const char * sort)
{
	Timer clock;
	double t1, t2;
	int *tab = new int [size];

	nazwain = std::to_string(size) + sort + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);
	
	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		delete[] tab;
		system("PAUSE");
		return 0;
	}

	for (int i = 0; !filein.eof(); i++)
		filein >> tab[i];

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	mergeSort(tab, 0, size);
	t2 = clock.GetCounter();

	if (!isSorted(tab, size))
	{
		std::cerr << "Tablica " << nazwain << " nieposortowana\n";
		system("PAUSE");
	}

	delete[] tab;
	return t2 - t1;
}

double Tester::tabq(const char * sort)
{
	Timer clock;
	double t1, t2;
	int *tab = new int[size];

	nazwain = std::to_string(size) + sort + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);

	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		delete[] tab;
		system("PAUSE");
		return 0;
	}

	for (int i = 0; !filein.eof(); i++)
		filein >> tab[i];

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	quickSort(tab, 0, size);
	t2 = clock.GetCounter();

	if (!isSorted(tab, size))
	{
		std::cerr << "Tablica " << nazwain << " nieposortowana\n";
		system("PAUSE");
	}
		

	delete[] tab;
	return t2 - t1;
}

double Tester::tabi(const char * sort)
{
	Timer clock;
	double t1, t2;
	int *tab = new int[size];

	nazwain = std::to_string(size) + sort + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);

	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		delete[] tab;
		system("PAUSE");
		return 0;
	}

	for (int i = 0; !filein.eof(); i++)
		filein >> tab[i];

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	introSort(tab, 0, size);
	t2 = clock.GetCounter();

	if (!isSorted(tab, size))
	{
		std::cerr << "Tablica " << nazwain << " nieposortowana\n";
		system("PAUSE");
	}

	delete[] tab;
	return t2 - t1;
}
