template<typename Type>
void quickSort(Type tab[], int front, int end)
{
	quickSortAlgorithm(tab, front, end - 1);
}

template <typename Type>
void quickSortAlgorithm(Type tab[], int front, int end)
{
	int middle;

	if (front != end) //je�li d�u�szy ni� 1 elem
	{
		middle = partition(tab, front, end); //we� punkt podzia�u, jako �rodek tablicy i podziel na cz�ci
		quickSortAlgorithm(tab, front, middle); //dla strony lewej
		quickSortAlgorithm(tab, middle + 1, end); //dla strony prawej
	}
}


template <typename Type>
void swapQuick(Type tab[], int &front, int &end)
{
	int temp = tab[end]; //tymaczasowa do zamieniania miejscami
	tab[end--] = tab[front];
	tab[front++] = temp;
}

template <typename Type>
int partition(Type tab[], int front, int end)
{
	//przyjmij punkt na �rodku jako por�wnywacz, w argumencie funkcja lambda (lekki przerost formy, ale to dla praktyki)
	int pivot = tab[([](int front, int end)->int { return (front + end) / 2; }(front, end))]; 
	int f = front, e = end; //indeksy tablic

	while (true)
	{
		for (e; tab[e] > pivot; e--); //dop�ki elementy sa wieksze od point
		for (f; tab[f] < pivot; f++); //dop�ki elementy sa mniejsze od point
		
		if (f < e) //zamie� miejscami
			swapQuick(tab, f, e);
		else //gdy sko�czy, zwr�� �rodek jako punkt podzia�u, przerwij p�tle i zako�cz
			return e;
	}
}
