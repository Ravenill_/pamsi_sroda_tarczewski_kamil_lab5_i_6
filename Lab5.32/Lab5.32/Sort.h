#pragma once
#include "MergeSort.h"
#include "IntroSort.h"
#include "QuickSort.h"

template <typename Type>
bool isSorted(Type tab[], int end)
{
	for (int i = 0; i < (end-1); i++)
	{
		if (tab[i] > tab[i + 1])
			return false;
		else
			continue;
	}
	return true;
}