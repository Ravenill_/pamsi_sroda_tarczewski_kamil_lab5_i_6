#pragma once
#include <fstream>
#include <string>

class Tester
{
private:
	int size;

	std::fstream filein;
	std::string nazwain;

public:
	Tester(int a);

	double tabm(const char *);
	double tabq(const char *);
	double tabi(const char *);
};